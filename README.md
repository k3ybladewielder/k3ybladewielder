[![author](https://img.shields.io/badge/author-alysson_guimarães-red.svg)](https://www.linkedin.com/in/guimaraesalysson/)


# Hello world ✨

![](https://github.com/k3ybladewielder/k3ybladewielder/blob/main/kamina.gif)

<tt>Me chamo Alysson, sou Bacharel em Administração formado pela Faculdade São Luís de França (2019) com MBA em Gestão de Negócios e Inteligência Competitiva pela Universidade Tiradentes(2020). Atualmente estudando ciência e engenharia de dados e aplicando em alguns projetos. Possuo +5 anos de experiência no varejo incluído atividades administrativas, data analyis e business intelligence na área de vendas.</tt>

## Tech Skills: 

<tt>Excel, Python, Power BI, Google Data Studio, Data Analysis, Business Intelligence, Data Science.</tt>

## Áreas de atuação:

<tt>Negócios, marketing, vendas, operações.</tt>

## Projetos<br>
<tt>* **Market K: [Business Analysis](https://github.com/k3ybladewielder/market_k/blob/main/market_k_eda.ipynb)**<br></tt>

<tt>* **Market K: [Market Basket Analysis](https://github.com/k3ybladewielder/market_k/blob/main/market_k_mba.ipynb)**<br></tt>

<tt>[Em construção 🚧🏗]</tt>


## Estudos<br>
<tt>* **[Bootcamp Analista de Machine Learning @ IGTI](https://github.com/k3ybladewielder/bootcamp_igti_ml)**<br></tt>

<tt>* **[Bootcamp Engenharia de Dados @ IGTI](https://github.com/k3ybladewielder/bootcamp_igti_ed)**<br></tt>


## Social
<tt>📧 alyssonalk@gmail.com<br></tt>
<tt>💼 [Linkedin](https://www.linkedin.com/in/guimaraesalysson/)</tt>
<tt>👨‍🚀 [Telegram](t.me/alysson)</tt>
<tt>🎙 [Spotify](https://open.spotify.com/user/sao5qyutaa7j64zwsojmyq7hq)</tt>
